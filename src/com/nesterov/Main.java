package com.nesterov;

public class Main {

    public static void main(String[] args) {
        int[][] graph = {{0, 1, 4, 0}, {0, 0, -3, 2}, {0, 0, 0, -5}, {0, 0, 0, 0}}; // Граф в виде матрицы смежности.
        int[] distances = new int[graph.length]; // Массив для хранения расстояний до вершины.

        int startVertex = 0; // Начальная вершина.

        bellmanFord(graph, distances, startVertex); // Вызов алгоритма Форда-Беллмана.

        for (int i = 0; i < distances.length; i++) {
            System.out.println("Расстояние от вершины " + startVertex + " до вершины " + i + " равно " + distances[i]);
        }
    }

    public static void bellmanFord(int[][] graph, int[] distances, int startVertex) {
        int numVertices = graph.length;

        // Инициализация расстояний до всех вершин как бесконечность.
        for (int i = 0; i < numVertices; i++) {
            distances[i] = Integer.MAX_VALUE;
        }

        // Расстояние от начальной вершины до самой себя равно 0.
        distances[startVertex] = 0;

        // Проход по всем рёбрам графа numVertices -1 раз.
        for (int i = 1; i < numVertices - 1; i++) {
            // Проход по всем рёбрам графа.
            for (int u = 0; u < numVertices; u++) {
                for (int v = 0; v < numVertices; v++) {
                    // Если есть ребро из u в v и расстояние от начальной вершины до v через u меньше текущего расстояния до v,
                    // то обновляем расстояние.
                    if (graph[u][v] != 0 && distances[u] != Integer.MAX_VALUE && distances[u] + graph[u][v] < distances[v]) {
                        distances[v] = distances[u] + graph[u][v];
                    }
                }
            }
        }
    }
}
